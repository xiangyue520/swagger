package com.wanggan.config;/**
 * Created by wanggan on 2017/7/15.
 */

import org.springframework.context.annotation.Configuration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * swagger config
 *
 * 如果正式运行注释或者去掉即可
 *
 * @author wanggan
 *
 * @create 2017-07-15 9:53
 **/
@Configuration
@EnableSwagger2
public class SwaggerConfig {

}
