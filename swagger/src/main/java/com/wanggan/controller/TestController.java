package com.wanggan.controller;/**
 * Created by wanggan on 2017/7/15.
 */

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 测试 controller
 *
 * @author wanggan
 *
 * @create 2017-07-15 9:55
 **/

@RestController
@RequestMapping("/test")
public class TestController {

    /**
     * 返回字符串
     *
     * @return
     */
    @RequestMapping("/hello")
    public String Hello(){
        return "hello, welcome to spring boot + swagger";
    }

    /**
     * 获取路径变量
     *
     * @param person
     *
     * @return
     */
    @RequestMapping("/{someone}")
    public String SomeOne(@PathVariable("someone") String person){
        return "hello "+person;
    }

}
